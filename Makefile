.PHONY : all
ROOT ?= imports
SOURCES = podem.py.nw

all : weave tangle

weave: $(SOURCES)
	noweave -n -delay -index podem.py.nw > ./doc/podem.tex
tangle: $(SOURCES)
	notangle -R"imports" podem.py.nw > ./podem

.PHONY: clean
doc_clean:
	rm -f *.log *.aux
full_clean:
	rm -f *.log *.aux *.tex podem
